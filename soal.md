<H1> Petunjuk : </H1>

MOHON DIBACA DAN DIPERHATIKAN UNTUK SEMUA CASE DIBAWAH INI

TYPE 1

1. Terdapat API public sangat sederhana 
   Buka https://github.com/HackerNews/API
    
2. Tugas kalian adalah 

    a. Tampilkan daftar TOP STORY dengan detail sebagai berikut :

        -  Masing-masing list memiliki atau harus menampilkan JUDUL, JUMLAH KOMENTAR, DAN SCORE 
        -  Ketika di Klik LIST tsb, Menampilkan DETAILNYA BESERTA TEKS KOMENTARNYA 

4. Buat progress dialog pada saat proses pengambilan data. (Progress Dialog tidak boleh diletakkan pada class view, cukup pemanggilannya saja di dalam class view)

5. Buat detail storinya ketika di klik..

6. Di dalam halaman detail terdapat button favorite yang terdapat function untuk menampilkan JUDUL FAVORITE DI HALAMAN UTAMA (HANYA JUDUL)

    INGAT TIDAK DIPERKENANKAN MENGGUNAKAN PREFERENCES ATAU LOCAL STORAGE LAIN

7. Untuk Detail gambaran aplikasinya dapat dilihat pada desain berikut ini :

    https://www.figma.com/file/RQ2etNstHVVe0HsbGrbTxY66/Untitled?node-id=0%3A1
    
8. MOHON DIBACA DAN DITELITI DAN DIPELAJARI PUBLIC API TERSEBUT HINGGA DAPAT MENAMPILKAN CASE SESUAI YANG DIINGINKAN.

9. Waktu pengerjaan default adalah 6 Jam, selesai atau tidak commit di Git Repository, Jika belum selesai lanjut saja hingga selesai dan commit lagi di Git Repository.
   Ingat 6 Jam commit dulu ya. kirim link repo github atau gitlab nya ke whatsapp nomor dibawah ini. atau ke email dibawah.

10. Jika ada yang ingin ditanyakan, silahkan kontak ke email panji.pam@gmail.com

11. Bahasa yang digunakan menggunakan KOTLIN 


* KETENTUAN PENGIRIMAN 
1. ZIP FOLDER PROJECT ANDA LALU UPLOAD DI GOOGLE DRIVE ATAU SEJENISNYA DAN KIRIM LINK TERSEBUT KE EMAIL : panji.pam@gmail.com
2. CLEAN TERLEBIH DAHULU PROJECT ANDA SEBELUM SUBMIT.
3. UPLOAD SOURCE CODE ANDA KE GIT REPOSITORY (GITHUB/GITLAB)
